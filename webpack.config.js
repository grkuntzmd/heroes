const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require("path");

module.exports = {
    entry: {
        app: [
            "./src/index.js"
        ]
    },

    output: {
        path: path.resolve(__dirname + "/dist"),
        filename: "[name].js"
    },

    module: {
        rules: [{
                test: /\.html$/,
                exclude: /node_modules/,
                loader: "file-loader?name=[name].[ext]"
            },
            {
                test: /\.elm$/,
                exclude: [/elm-stuff/, /node_modules/],
                loader: "elm-webpack-loader?verbose=true&warn=true"
            },
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "url-loader?limit=10000&mimetype=application/font-woff"
            },
            {
                test: /\.(ttf|eot|svg|ico)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "file-loader"
            },
            {
                test: /\.css$/,
                use: [
                    "style-loader",
                    "css-loader"
                ]
            }
        ],

        noParse: /\.elm$/
    },

    devServer: {
        inline: true,
        stats: { colors: true }
    },

    plugins: [
        new CopyWebpackPlugin([
            { from: "node_modules/dialog-polyfill/dialog-polyfill.css" },
            { from: "node_modules/dialog-polyfill/dialog-polyfill.js" },
            { from: "src/favicon.ico" }
        ])
    ]
};