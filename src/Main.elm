module Main exposing (main)

import Abilities
import Attributes
import Background
import Body
import Css
    exposing
        ( alignItems
        , auto
        , bottom
        , center
        , column
        , display
        , displayFlex
        , fixed
        , flex
        , flexDirection
        , height
        , inlineBlock
        , int
        , justifyContent
        , margin
        , paddingRight
        , pct
        , position
        , px
        , row
        , spaceBetween
        , stretch
        , width
        )
import Debug
import Demographics
import Header
import Html exposing (Html, div, span, text)
import Html.Attributes exposing (..)
import InitializeSeeds exposing (initializeSeeds)
import Model exposing (Model, init)
import Msg exposing (Msg(..))
import Random.Pcg as Random
import Styles exposing (stretchFlexColumn, styles)
import Task


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , update = update
        , subscriptions = always Sub.none
        , view = view
        }



-- Update


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    Tuple.second <|
        Debug.log "Main msg, update"
            ( msg
            , case msg of
                AbilitiesMsg msg_ ->
                    let
                        ( model_, cmd, upstreamMsg ) =
                            Abilities.update msg_ model.abilities

                        res =
                            (\m v ->
                                ( { model | abilities = model_ }
                                , Cmd.batch
                                    [ Cmd.map AbilitiesMsg cmd
                                    , Cmd.map AttributesMsg
                                        (Task.perform m (Task.succeed v))
                                    ]
                                )
                            )
                    in
                        case upstreamMsg of
                            Abilities.RollDamageUp value ->
                                res Attributes.RollDamage value

                            Abilities.RollStrengthUp value ->
                                res Attributes.RollStrength value

                            Abilities.RollDexterityUp value ->
                                res Attributes.RollDexterity value

                            Abilities.RollConstitutionUp value ->
                                res Attributes.RollConstitution value

                            Abilities.RollIntelligenceUp value ->
                                res Attributes.RollIntelligence value

                            Abilities.RollWisdomUp value ->
                                res Attributes.RollWisdom value

                            Abilities.RollCharismaUp value ->
                                res Attributes.RollCharisma value

                            Abilities.NoneUp ->
                                ( { model | abilities = model_ }
                                , Cmd.map AbilitiesMsg cmd
                                )

                AttributesMsg msg_ ->
                    let
                        ( model_, cmd ) =
                            Attributes.update msg_ model.attributes
                    in
                        ( { model | attributes = model_ }
                        , Cmd.map AttributesMsg cmd
                        )

                BackgroundMsg msg_ ->
                    let
                        ( model_, cmd ) =
                            Background.update msg_ model.background
                    in
                        ( { model | background = model_ }
                        , Cmd.map BackgroundMsg cmd
                        )

                DemographicsMsg msg_ ->
                    let
                        ( model_, cmd ) =
                            Demographics.update msg_ model.demographics
                    in
                        ( { model | demographics = model_ }
                        , Cmd.map DemographicsMsg cmd
                        )

                EditGlobalItems ->
                    ( model, Cmd.none )

                SyncToCloud ->
                    ( model, Cmd.none )

                InitializeSeeds seed ->
                    let
                        msgNames =
                            [ AttributesMsg << Attributes.InitializeSeed
                            , BackgroundMsg << Background.InitializeSeeds
                            ]
                    in
                        ( initializeSeeds (Random.initialSeed seed)
                            msgNames
                            update
                            model
                        , Cmd.none
                        )
            )



-- View


view : Model -> Html Msg
view model =
    div
        [ stretchFlexColumn ++ [ Css.height (pct 100) ] |> styles ]
        [ Header.view model
        , Body.view model
        , div
            [ class "w3-bar w3-theme"
            , styles
                [ bottom (px 0)
                , flex (int 0)
                , position fixed
                , Css.width (pct 100)
                ]
            ]
            [ span
                [ class "w3-bar-item" ]
                [ text "Copyright © 2017, G. Ralph Kuntz, MD" ]
            ]
        ]
