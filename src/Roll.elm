module Roll exposing (check, evaluate)

import Char exposing (isDigit)
import Parser exposing (..)
import Random.Pcg exposing (Seed, list, step)
import Random.Pcg as Random


type Operation
    = Add Roll
    | Sub Roll


type Roll
    = All Dice
    | Best Dice
    | Worst Dice
    | Number Int


type alias Dice =
    { count : Int
    , size : Int
    }


dice : Parser Dice
dice =
    delayedCommitMap Dice
        (succeed identity
            |= count
            |. spaces
        )
        (succeed identity
            |. keyword "d"
            |. spaces
            |= integer
        )


count : Parser Int
count =
    oneOf
        [ integer |> andThen succeed
        , succeed 1
        ]


integer : Parser Int
integer =
    keep oneOrMore isDigit
        |> andThen
            (\s ->
                case
                    String.toInt s
                        |> Result.andThen
                            (\i ->
                                if i >= 1 then
                                    Ok i
                                else
                                    Err "Must be greater than 0"
                            )
                of
                    Ok value ->
                        succeed value

                    Err err ->
                        fail err
            )


all : Parser Roll
all =
    succeed All
        |= dice


best : Parser Roll
best =
    succeed Best
        |. keyword "b"
        |. spaces
        |. symbol "["
        |. spaces
        |= dice
        |. spaces
        |. symbol "]"


worst : Parser Roll
worst =
    succeed Worst
        |. keyword "w"
        |. spaces
        |. symbol "["
        |. spaces
        |= dice
        |. spaces
        |. symbol "]"


roll : Parser Roll
roll =
    oneOf
        [ all
        , best
        , worst
        , int |> andThen (Number >> succeed)
        ]


expr : Parser (List Operation)
expr =
    succeed identity
        |= andThen (\r -> exprHelp [ r ]) (succeed Add |= roll)
        |. spaces
        |. end


exprHelp : List Operation -> Parser (List Operation)
exprHelp revOps =
    oneOf
        [ nextRoll |> andThen (\r -> exprHelp (r :: revOps))
        , succeed (List.reverse revOps)
        ]


nextRoll : Parser Operation
nextRoll =
    delayedCommit spaces <|
        oneOf
            [ succeed Add
                |. symbol "+"
                |. spaces
                |= roll
            , succeed Sub
                |. symbol "-"
                |. spaces
                |= roll
            ]


spaces : Parser ()
spaces =
    ignore zeroOrMore (\c -> c == ' ')


evaluate : String -> Seed -> Result Error ( String, Seed )
evaluate command seed =
    String.toLower command
        |> run expr
        |> Result.map
            (List.foldl
                (\op ( outputs, acc, seed_ ) ->
                    case op of
                        Add roll ->
                            let
                                ( output, val, newSeed ) =
                                    evalRoll roll seed_
                            in
                                ( ("+ " ++ output) :: outputs, acc + val, newSeed )

                        Sub roll ->
                            let
                                ( output, val, newSeed ) =
                                    evalRoll roll seed_
                            in
                                ( ("- " ++ output) :: outputs, acc - val, newSeed )
                )
                ( [], 0, seed )
            )
        |> Result.map
            (\( outputs, total, seed_ ) ->
                ( (List.reverse outputs |> String.join " ")
                    ++ (" = **" ++ toString total ++ "**")
                    |> String.dropLeft 2
                , seed_
                )
            )


evalRoll : Roll -> Seed -> ( String, Int, Seed )
evalRoll roll_ seed =
    case roll_ of
        All value ->
            let
                ( values, seed_ ) =
                    step (list value.count (Random.int 1 value.size)) seed

                sum =
                    List.sum values

                output =
                    "[ "
                        ++ (List.map toString values |> String.join ", ")
                        ++ " ]"
            in
                ( output, sum, seed_ )

        Best value ->
            let
                ( values, seed_ ) =
                    step (list value.count (Random.int 1 value.size)) seed

                max =
                    List.maximum values |> Maybe.withDefault 0

                output =
                    "**b**[ "
                        ++ (highlightFirst max values
                                |> String.join ", "
                           )
                        ++ " ]"
            in
                ( output, max, seed_ )

        Worst value ->
            let
                ( values, seed_ ) =
                    step (list value.count (Random.int 1 value.size)) seed

                min =
                    List.minimum values |> Maybe.withDefault 0

                output =
                    "**w**[ "
                        ++ (highlightFirst min values
                                |> String.join ", "
                           )
                        ++ " ]"
            in
                ( output, min, seed_ )

        Number value ->
            let
                output =
                    toString value
            in
                ( output, value, seed )


check : String -> Result Error (List Operation)
check command =
    String.toLower command
        |> run expr


highlightFirst : a -> List a -> List String
highlightFirst target values =
    let
        result =
            List.foldl
                (\cur ( found, acc ) ->
                    if not found && cur == target then
                        ( True, ("**" ++ toString cur ++ "**") :: acc )
                    else
                        ( found, toString cur :: acc )
                )
                ( False, [] )
                values
    in
        Tuple.second result |> List.reverse
