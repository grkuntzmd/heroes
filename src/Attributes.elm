module Attributes exposing (Model, Msg(..), initialModel, update, view)

import Css
    exposing
        ( alignItems
        , auto
        , backgroundColor
        , center
        , displayFlex
        , em
        , ex
        , flex
        , flexDirection
        , int
        , justifyContent
        , margin2
        , maxHeight
        , maxWidth
        , minHeight
        , overflowY
        , px
        , rgb
        , row
        , spaceBetween
        )
import Dom.Scroll exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (keyCode, on, onClick, onInput)
import Inputs exposing (grayInputNumber)
import Json.Decode as Json
import Markdown
import Markdown.Config exposing (defaultOptions)
import MarkdownDisplay
import Nbsp exposing (nbsp)
import Random.Pcg exposing (Seed, initialSeed)
import Roll exposing (check, evaluate)
import Styles
    exposing
        ( centerFlexRow
        , centerFlexRowBetween
        , chevronDown
        , chevronUp
        , icon
        , stretchFlexRow
        , styles
        )
import Task


type alias Model =
    { alignmentDrive : MarkdownDisplay.Model
    , alignmentDriveExpand : Bool
    , bondsFlags : MarkdownDisplay.Model
    , bondsFlagsExpand : Bool
    , gear : MarkdownDisplay.Model
    , gearCurrentLoad : Int
    , gearMaxLoad : Int
    , gearExpand : Bool
    , notes : MarkdownDisplay.Model
    , notesExpand : Bool
    , diceRolls : String
    , rollCommand : String
    , rollCommandError : Bool
    , seed : Seed
    }


initialModel : Model
initialModel =
    { alignmentDrive = MarkdownDisplay.initialModel
    , alignmentDriveExpand = False
    , bondsFlags = MarkdownDisplay.initialModel
    , bondsFlagsExpand = False
    , gear = MarkdownDisplay.initialModel
    , gearCurrentLoad = 0
    , gearMaxLoad = 0
    , gearExpand = False
    , notes = MarkdownDisplay.initialModel
    , notesExpand = False
    , diceRolls = ""
    , rollCommand = ""
    , rollCommandError = False
    , seed = initialSeed 0
    }


type Msg
    = AlignmentDriveMsg MarkdownDisplay.Msg
    | AlignmentDriveExpand
    | BondsFlagsMsg MarkdownDisplay.Msg
    | BondsFlagsExpand
    | GearMsg MarkdownDisplay.Msg
    | GearCurrentLoad Int
    | GearMaxLoad Int
    | GearExpand
    | NotesMsg MarkdownDisplay.Msg
    | NotesExpand
    | RollCommand String
    | RollKey Int
    | RunRoll
    | DiceRollsClear
    | RollDamage String
    | RollStrength Int
    | RollDexterity Int
    | RollConstitution Int
    | RollIntelligence Int
    | RollWisdom Int
    | RollCharisma Int
    | NoOp
    | InitializeSeed Seed


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AlignmentDriveMsg msg_ ->
            let
                ( updatedModel, cmd ) =
                    MarkdownDisplay.update msg_ model.alignmentDrive
            in
                ( { model | alignmentDrive = updatedModel }
                , Cmd.map AlignmentDriveMsg cmd
                )

        AlignmentDriveExpand ->
            ( { model | alignmentDriveExpand = not model.alignmentDriveExpand }
            , Cmd.none
            )

        BondsFlagsMsg msg_ ->
            let
                ( updatedModel, cmd ) =
                    MarkdownDisplay.update msg_ model.bondsFlags
            in
                ( { model | bondsFlags = updatedModel }
                , Cmd.map BondsFlagsMsg cmd
                )

        BondsFlagsExpand ->
            ( { model | bondsFlagsExpand = not model.bondsFlagsExpand }
            , Cmd.none
            )

        GearMsg msg_ ->
            let
                ( updatedModel, cmd ) =
                    MarkdownDisplay.update msg_ model.gear
            in
                ( { model | gear = updatedModel }, Cmd.map GearMsg cmd )

        GearCurrentLoad value ->
            ( { model | gearCurrentLoad = value }, Cmd.none )

        GearMaxLoad value ->
            ( { model | gearMaxLoad = value }, Cmd.none )

        GearExpand ->
            ( { model | gearExpand = not model.gearExpand }, Cmd.none )

        NotesMsg msg_ ->
            let
                ( updatedModel, cmd ) =
                    MarkdownDisplay.update msg_ model.notes
            in
                ( { model | notes = updatedModel }, Cmd.map NotesMsg cmd )

        NotesExpand ->
            ( { model | notesExpand = not model.notesExpand }, Cmd.none )

        RollCommand value ->
            let
                error =
                    if String.length value > 0 then
                        case check value of
                            Ok _ ->
                                False

                            Err _ ->
                                True
                    else
                        False
            in
                ( { model | rollCommand = value, rollCommandError = error }
                , Cmd.none
                )

        RollKey key ->
            if key == 13 then
                update RunRoll model
            else
                ( model, Cmd.none )

        RunRoll ->
            let
                ( model_, cmd ) =
                    roll model.rollCommand model model.rollCommand
            in
                ( { model_ | rollCommand = "" }, cmd )

        DiceRollsClear ->
            ( { model | diceRolls = "" }, Cmd.none )

        RollDamage value ->
            roll value model "DMG"

        RollStrength value ->
            rollStat value model "STR"

        RollDexterity value ->
            rollStat value model "DEX"

        RollConstitution value ->
            rollStat value model "CON"

        RollIntelligence value ->
            rollStat value model "INT"

        RollWisdom value ->
            rollStat value model "WIS"

        RollCharisma value ->
            rollStat value model "CHA"

        NoOp ->
            ( model, Cmd.none )

        InitializeSeed seed_ ->
            ( { model | seed = seed_ }, Cmd.none )


roll : String -> Model -> String -> ( Model, Cmd Msg )
roll expr model label =
    let
        result =
            evaluate expr model.seed
    in
        case result of
            Ok ( output, seed ) ->
                ( { model
                    | seed = seed
                    , diceRolls =
                        model.diceRolls
                            ++ "\n**"
                            ++ label
                            ++ "**: "
                            ++ output
                  }
                , Task.attempt (always NoOp) <| toBottom "dice-rolls"
                )

            Err err ->
                ( { model
                    | diceRolls =
                        model.diceRolls
                            ++ "\nBadly formed roll: "
                            ++ expr
                  }
                , Cmd.none
                )


rollStat : Int -> Model -> String -> ( Model, Cmd Msg )
rollStat stat model label =
    let
        expr =
            if stat >= 0 then
                "2d6 + " ++ (toString stat)
            else
                "2d6 - " ++ (negate stat |> toString)
    in
        roll expr model label


view : Model -> Html Msg
view model =
    div [ class "w3-margin-top" ]
        [ div
            []
            [ alignmentDrive model
            , bondsFlags model
            , gear model
            , notes model
            ]
        , rolls model
        ]


alignmentDrive : Model -> Html Msg
alignmentDrive model =
    let
        ( class_, icon_ ) =
            if model.alignmentDriveExpand then
                ( "", chevronUp )
            else
                ( "w3-hide", chevronDown )
    in
        div
            []
            [ heading "Alignment / Drive" AlignmentDriveExpand icon_
            , div [ class class_ ]
                [ Html.map AlignmentDriveMsg
                    (MarkdownDisplay.view "Alignment / Drive"
                        model.alignmentDrive
                    )
                ]
            ]


bondsFlags : Model -> Html Msg
bondsFlags model =
    let
        ( class_, icon_ ) =
            if model.bondsFlagsExpand then
                ( "", chevronUp )
            else
                ( "w3-hide", chevronDown )
    in
        div
            [ class "w3-margin-top" ]
            [ heading "Bonds / Flags" BondsFlagsExpand icon_
            , div [ class class_ ]
                [ Html.map BondsFlagsMsg
                    (MarkdownDisplay.view "Bonds / Flags" model.bondsFlags)
                ]
            ]


gear : Model -> Html Msg
gear model =
    let
        ( class_, icon_ ) =
            if model.gearExpand then
                ( "", chevronUp )
            else
                ( "w3-hide", chevronDown )

        styles_ =
            styles [ maxHeight (ex 4), maxWidth (Css.em 4) ]
    in
        div
            [ class "w3-margin-top" ]
            [ header
                [ class "w3-padding-small w3-theme"
                , styles centerFlexRowBetween
                ]
                [ span [] [ text "Gear" ]
                , span
                    [ styles centerFlexRow
                    ]
                    [ label [ class "w3-margin-right" ] [ text "Load" ]
                    , grayInputNumber GearCurrentLoad
                        Nothing
                        model.gearCurrentLoad
                        (Just styles_)
                    , span
                        [ styles [ margin2 (px 0) (px 4) ] ]
                        [ text "/" ]
                    , grayInputNumber GearMaxLoad
                        Nothing
                        model.gearMaxLoad
                        (Just styles_)
                    ]
                , button
                    [ class "w3-button w3-circle w3-padding-small w3-ripple w3-tiny"
                    , onClick GearExpand
                    ]
                    [ icon_ ]
                ]
            , div [ class class_ ]
                [ Html.map GearMsg (MarkdownDisplay.view "Gear" model.gear)
                ]
            ]


notes : Model -> Html Msg
notes model =
    let
        ( class_, icon_ ) =
            if model.notesExpand then
                ( "", chevronUp )
            else
                ( "w3-hide", chevronDown )
    in
        div
            [ class "w3-margin-top" ]
            [ heading "Notes" NotesExpand icon_
            , div [ class class_ ]
                [ Html.map NotesMsg (MarkdownDisplay.view "Notes" model.notes) ]
            ]


rolls : Model -> Html Msg
rolls model =
    let
        inputStyles_ =
            if model.rollCommandError then
                [ (backgroundColor (rgb 255 192 203)), flex (int 1) ]
            else
                [ flex (int 1) ]
    in
        div
            [ class "w3-margin-top" ]
            [ header
                [ class "w3-padding-small w3-theme"
                , styles centerFlexRowBetween
                ]
                [ span [] [ text "Dice Rolls" ]
                , span
                    [ class "w3-button w3-padding-small"
                    , onClick DiceRollsClear
                    ]
                    [ icon "trash" ]
                ]
            , div
                [ class "w3-border"
                , id "dice-rolls"
                , styles
                    [ maxHeight (ex 10)
                    , minHeight (ex 10)
                    , overflowY auto
                    ]
                ]
              <|
                Markdown.toHtml
                    (Just { defaultOptions | softAsHardLineBreak = True })
                    model.diceRolls
            , span
                [ styles
                    [ alignItems center
                    , displayFlex
                    , flexDirection row
                    , justifyContent spaceBetween
                    ]
                ]
                [ label
                    [ class "w3-margin-right"
                    , styles [ flex (int 0) ]
                    ]
                    [ nbsp "Roll (e.g. \"1d4 + 2\")" |> text ]
                , input
                    [ onInput RollCommand
                    , onKeyDown RollKey
                    , styles inputStyles_
                    , value model.rollCommand
                    ]
                    []
                , a
                    [ class "w3-button w3-large"
                    , onClick RunRoll
                    , styles [ flex (int 0) ]
                    ]
                    [ text "↵" ]
                ]
            ]


heading : String -> Msg -> Html Msg -> Html Msg
heading title msg icon =
    header
        [ class "w3-padding-small w3-theme", styles centerFlexRowBetween ]
        [ span [] [ text title ]
        , button
            [ class "w3-button w3-circle w3-padding-small w3-ripple w3-tiny"
            , onClick msg
            ]
            [ icon ]
        ]


onKeyDown : (Int -> msg) -> Attribute msg
onKeyDown tagger =
    on "keydown" (Json.map tagger keyCode)
