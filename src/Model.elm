module Model exposing (Model, init)

import Abilities
import Attributes
import Background
import Demographics
import Msg exposing (Msg(..))
import Random.Pcg exposing (generate, int, maxInt, minInt)


type alias Model =
    { abilities : Abilities.Model
    , attributes : Attributes.Model
    , background : Background.Model
    , demographics : Demographics.Model
    }


initialModel : Model
initialModel =
    { abilities = Abilities.initialModel
    , attributes = Attributes.initialModel
    , background = Background.initialModel
    , demographics = Demographics.initialModel
    }


init : ( Model, Cmd Msg )
init =
    ( initialModel, generate (InitializeSeeds) (int minInt maxInt) )
