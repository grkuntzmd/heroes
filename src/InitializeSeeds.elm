module InitializeSeeds exposing (initializeSeeds)

import Random.Pcg as Random


initializeSeeds :
    Random.Seed
    -> List (Random.Seed -> msg)
    -> (msg -> model -> ( model, Cmd msg ))
    -> model
    -> model
initializeSeeds seed msgNames update model =
    let
        seeds =
            Random.step
                (Random.list (List.length msgNames)
                    Random.independentSeed
                )
                seed
                |> Tuple.first
    in
        List.map2 (<|) msgNames seeds
            |> List.foldl
                (\msg mod ->
                    update msg mod
                        |> Tuple.first
                )
                model
