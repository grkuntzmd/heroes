module Msg exposing (Msg(..))

import Abilities
import Attributes
import Background
import Demographics


type Msg
    = AbilitiesMsg Abilities.Msg
    | AttributesMsg Attributes.Msg
    | BackgroundMsg Background.Msg
    | DemographicsMsg Demographics.Msg
    | EditGlobalItems
    | SyncToCloud
    | InitializeSeeds Int
