module Demographics exposing (Model, Msg, initialModel, update, view)

import Css
    exposing
        ( Style
        , alignItems
        , column
        , displayFlex
        , flex
        , flexDirection
        , flexWrap
        , hidden
        , int
        , justifyContent
        , marginTop
        , px
        , row
        , spaceBetween
        , stretch
        , wrap
        )
import Html exposing (..)
import Html.Attributes exposing (..)
import Inputs exposing (grayInputNumber, grayInputText)
import MarkdownDisplay
import Maybe exposing (withDefault)
import Nbsp exposing (nbsp)
import Styles exposing (stretchFlexColumn, stretchFlexRow, styles)


type alias Model =
    { name : String
    , class : String
    , race : String
    , level : Int
    , currentXP : Int
    , look : MarkdownDisplay.Model
    }


initialModel : Model
initialModel =
    { name = ""
    , class = ""
    , race = ""
    , level = 1
    , currentXP = 0
    , look = MarkdownDisplay.initialModel
    }


type Msg
    = Name String
    | Class String
    | Race String
    | Level Int
    | CurrentXP Int
    | LookMsg MarkdownDisplay.Msg
    | Noop String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Name value ->
            ( { model | name = value }, Cmd.none )

        Class value ->
            ( { model | class = value }, Cmd.none )

        Race value ->
            ( { model | race = value }, Cmd.none )

        Level value ->
            ( { model | level = value }, Cmd.none )

        CurrentXP value ->
            ( { model | currentXP = value }, Cmd.none )

        LookMsg msg_ ->
            let
                ( model_, cmd ) =
                    MarkdownDisplay.update msg_ model.look
            in
                ( { model | look = model_ }, Cmd.map LookMsg cmd )

        Noop _ ->
            ( model, Cmd.none )


view : Model -> Html Msg
view model =
    div
        [ styles stretchFlexColumn ]
        [ name model
        , classRace model
        , level model
        , look model
        ]


name : Model -> Html Msg
name model =
    span
        [ styles rowStyle ]
        [ smallLabel "Name"
        , grayInputText Name model.name Nothing
        ]


classRace : Model -> Html Msg
classRace model =
    span
        [ styles rowStyle ]
        [ smallLabel "Class"
        , grayInputText Class model.class Nothing
        , smallLabel "Race"
        , grayInputText Race model.race Nothing
        ]


level : Model -> Html Msg
level model =
    span
        [ styles (rowStyle ++ [ justifyContent spaceBetween ]) ]
        [ smallLabel "Level"
        , grayInputNumber Level (Just 10) model.level Nothing
        , nbsp "Current XP" |> smallLabel
        , grayInputNumber CurrentXP Nothing model.currentXP Nothing
        , nbsp "XP to Level" |> smallLabel
        , grayInputText Noop (model.level + 7 |> toString) (readonly True |> Just)
        ]


look : Model -> Html Msg
look model =
    div
        [ styles rowStyle ]
        [ smallLabel "Look"
        , Html.map LookMsg (MarkdownDisplay.view "Look" model.look)
        ]


rowStyle : List Style
rowStyle =
    stretchFlexRow ++ [ marginTop (px 4) ]


smallLabel : String -> Html Msg
smallLabel string =
    label [ class "w3-padding-small" ] [ text string ]
