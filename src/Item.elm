module Item
    exposing
        ( Model
        , Msg
        , UpstreamMsg(..)
        , find
        , initialModel
        , remove
        , replace
        , update
        , view
        )

import Css
    exposing
        ( alignItems
        , auto
        , backgroundColor
        , center
        , displayFlex
        , em
        , ex
        , flex
        , flexDirection
        , int
        , justifyContent
        , margin2
        , maxHeight
        , maxWidth
        , minHeight
        , overflowY
        , px
        , rgb
        , row
        , spaceBetween
        )
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (on, onClick, onInput)
import Inputs exposing (grayInputText)
import List.Extra as Extra
import MarkdownDisplay
import Styles
    exposing
        ( centerFlexRow
        , centerFlexRowBetween
        , chevronDown
        , chevronUp
        , icon
        , marginTopSmall
        , stretchFlexColumn
        , styles
        )


type alias Model =
    { name : String
    , contents : MarkdownDisplay.Model
    , uuid : String
    , expand : Bool
    }


initialModel : String -> Model
initialModel uuid =
    { name = uuid
    , contents = MarkdownDisplay.initialModel
    , uuid = uuid
    , expand = False
    }


type Msg
    = Name String
    | Contents MarkdownDisplay.Msg
    | DeleteItem
    | Expand
    | Raise
    | Lower


type UpstreamMsg
    = ReplaceItemUp
    | DeleteItemUp
    | RaiseUp
    | LowerUp


update : Msg -> Model -> ( Model, Cmd Msg, UpstreamMsg )
update msg model =
    case msg of
        Name value ->
            ( { model | name = value }, Cmd.none, ReplaceItemUp )

        Contents msg_ ->
            let
                ( model_, cmd ) =
                    MarkdownDisplay.update msg_ model.contents
            in
                ( { model | contents = model_ }
                , Cmd.map Contents cmd
                , ReplaceItemUp
                )

        DeleteItem ->
            ( model, Cmd.none, DeleteItemUp )

        Expand ->
            ( { model | expand = not model.expand }
            , Cmd.none
            , ReplaceItemUp
            )

        Raise ->
            ( model, Cmd.none, RaiseUp )

        Lower ->
            ( model, Cmd.none, LowerUp )


view : Model -> Bool -> Bool -> Html Msg
view model disabledUp disabledDown =
    let
        ( class_, icon_ ) =
            if model.expand then
                ( "", chevronUp )
            else
                ( "w3-hide", chevronDown )

        nameStyles_ =
            styles [ maxHeight (ex 4) ]
    in
        div
            [ marginTopSmall ++ stretchFlexColumn |> styles
            ]
            [ header
                [ class "w3-padding-small w3-theme"
                , styles centerFlexRowBetween
                ]
                [ div [ styles centerFlexRow ]
                    [ button
                        [ class "w3-button w3-circle w3-padding-small w3-ripple w3-tiny"
                        , disabled disabledUp
                        , onClick Raise
                        ]
                        [ icon "arrow-circle-up" ]
                    , button
                        [ class "w3-button w3-circle w3-padding-small w3-ripple w3-tiny"
                        , disabled disabledDown
                        , onClick Lower
                        ]
                        [ icon "arrow-circle-down" ]
                    , button
                        [ class "w3-button w3-circle w3-margin-left w3-margin-right w3-padding-small w3-ripple w3-tiny"
                        , onClick Expand
                        ]
                        [ icon_ ]
                    , span []
                        [ grayInputText Name model.name (Just nameStyles_) ]
                    ]
                , span
                    [ class "w3-button w3-circle w3-padding-small w3-right w3-ripple w3-tiny", onClick DeleteItem ]
                    [ icon "trash" ]
                ]
            , div [ class class_ ]
                [ Html.map Contents
                    (MarkdownDisplay.view model.name model.contents)
                ]
            ]


find : String -> List Model -> Maybe ( Int, Model )
find uuid items =
    List.indexedMap (,) items
        |> Extra.find (\i -> (Tuple.second i).uuid == uuid)


replace : Model -> List Model -> List Model
replace item items =
    List.map
        (\i ->
            if i.uuid == item.uuid then
                item
            else
                i
        )
        items


remove : Model -> List Model -> List Model
remove item items =
    List.foldl
        (\i acc ->
            if i.uuid == item.uuid then
                acc
            else
                i :: acc
        )
        []
        items
        |> List.reverse
