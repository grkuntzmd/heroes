module Inputs exposing (grayInputNumber, grayInputText)

import Html exposing (..)
import Html.Attributes exposing (class)
import Html.Events exposing (onInput)
import Input.Number as Number
import Input.Text as Text
import Maybe exposing (withDefault)


grayInputNumber :
    (Int -> msg)
    -> Maybe Int
    -> Int
    -> Maybe (Attribute msg)
    -> Html msg
grayInputNumber msg maybeMaxValue value maybeStyles =
    Number.input
        { hasFocus = Nothing
        , maxLength = Nothing
        , maxValue = maybeMaxValue
        , minValue = Just 0
        , onInput = withDefault 0 >> msg
        }
        ([ inputClasses ]
            ++ case maybeStyles of
                Just value ->
                    [ value ]

                Nothing ->
                    []
        )
        (Just value)


grayInputText : (String -> msg) -> String -> Maybe (Attribute msg) -> Html msg
grayInputText msg value maybeStyles =
    Text.input
        { hasFocus = Nothing
        , maxLength = Just 50
        , onInput = msg
        , type_ = "text"
        }
        (let
            attrs =
                [ inputClasses, onInput msg ]
         in
            case maybeStyles of
                Just value ->
                    value :: attrs

                Nothing ->
                    attrs
        )
        value


inputClasses : Attribute msg
inputClasses =
    class "w3-border w3-hover-gray w3-input"
