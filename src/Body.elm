module Body exposing (view)

import Abilities
import Attributes
import Background
import Css
    exposing
        ( alignItems
        , auto
        , displayFlex
        , flex
        , flexDirection
        , int
        , marginBottom
        , marginTop
        , minWidth
        , px
        , row
        , stretch
        )
import Demographics
import Html exposing (..)
import Html.Attributes exposing (..)
import Model exposing (Model)
import Msg exposing (Msg(..))
import Styles exposing (centerFlexRow, startFlexRow, styles)


view : Model -> Html Msg
view model =
    div
        [ startFlexRow
            ++ [ marginBottom (px 40)
               , marginTop (px 70)
               ]
            |> styles
        ]
        [ div
            [ class "w3-padding-small"
            , styles
                [ flex (int 0)
                , minWidth (px 450)
                ]
            ]
            [ Html.map DemographicsMsg (Demographics.view model.demographics)
            , Html.map AttributesMsg (Attributes.view model.attributes)
            ]
        , div
            [ class "w3-padding-small", styles [ flex (int 1) ] ]
            [ Html.map AbilitiesMsg (Abilities.view model.abilities)
            , Html.map BackgroundMsg (Background.view model.background)
            ]
        ]
