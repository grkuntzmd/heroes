module MarkdownDisplay exposing (Model, Msg, initialModel, update, view)

import Css
    exposing
        ( alignItems
        , auto
        , block
        , center
        , column
        , display
        , displayFlex
        , ex
        , flex
        , flexDirection
        , hidden
        , inlineBlock
        , int
        , justifyContent
        , margin
        , maxHeight
        , minHeight
        , none
        , overflowY
        , paddingRight
        , pct
        , property
        , px
        , row
        , spaceBetween
        , stretch
        , visible
        , width
        )
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Markdown
import Markdown.Config exposing (defaultOptions)
import Styles exposing (styles)


type alias Model =
    { contents : String
    , editing : Bool
    }


initialModel : Model
initialModel =
    { contents = ""
    , editing = False
    }


type Msg
    = Editing
    | NewContent String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Editing ->
            ( { model | editing = not model.editing }, Cmd.none )

        NewContent value ->
            ( { model | contents = value }, Cmd.none )


view : String -> Model -> Html Msg
view title model =
    let
        display_ =
            if model.editing then
                [ display block, Css.property "visibility" "visible" ]
            else
                [ display none, Css.property "visibility" "hidden" ]
    in
        div
            [ class "w3-border", styles [ flex (int 1) ] ]
            [ div
                [ class "w3-padding-small"
                , onClick Editing
                , styles
                    [ maxHeight (ex 20)
                    , minHeight (ex 10)
                    , overflowY auto
                    ]
                ]
              <|
                Markdown.toHtml
                    (Just { defaultOptions | softAsHardLineBreak = True })
                    model.contents
            , div
                [ class "w3-modal"
                , styles display_
                ]
                [ div
                    [ class "w3-animate-zoom w3-modal-content" ]
                    [ div
                        [ class "w3-container w3-theme" ]
                        [ h3 [] [ "Editing " ++ title |> text ]
                        , span
                            [ class "w3-button w3-display-topright"
                            , onClick Editing
                            ]
                            [ text "×" ]
                        , div
                            [ class "w3-margin" ]
                            [ textarea
                                [ onInput NewContent
                                , rows 20
                                , styles
                                    [ Css.width (pct 100)
                                    ]
                                ]
                                [ text model.contents ]
                            ]
                        , span
                            [ class "w3-button w3-right"
                            , onClick Editing
                            ]
                            [ text "Ok" ]
                        ]
                    ]
                ]
            ]
