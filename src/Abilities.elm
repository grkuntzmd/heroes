module Abilities
    exposing
        ( Model
        , Msg
        , UpstreamMsg(..)
        , initialModel
        , update
        , view
        )

import Css
    exposing
        ( alignItems
        , auto
        , backgroundColor
        , block
        , border3
        , borderRadius
        , center
        , column
        , display
        , displayFlex
        , ex
        , flex
        , flexDirection
        , flexEnd
        , flexWrap
        , fontSize
        , height
        , hidden
        , int
        , justifyContent
        , lineHeight
        , margin
        , margin2
        , marginLeft
        , marginRight
        , marginTop
        , maxHeight
        , maxWidth
        , minWidth
        , padding
        , pct
        , px
        , rgb
        , row
        , solid
        , spaceAround
        , spaceBetween
        , stretch
        , textAlign
        , width
        )
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Inputs exposing (grayInputNumber, grayInputText)
import Maybe exposing (withDefault)
import Roll exposing (check)
import Styles
    exposing
        ( centerFlexColumn
        , centerFlexRow
        , centerFlexRowAround
        , checkboxStyles
        , icon
        , stretchFlexColumn
        , stretchFlexRow
        , styles
        )


type alias Model =
    { strength : Int
    , dexterity : Int
    , constitution : Int
    , intelligence : Int
    , wisdom : Int
    , charisma : Int
    , damage : String
    , damageError : Bool
    , armor : Int
    , currentHP : Int
    , maxHP : Int
    , weak : Bool
    , shaky : Bool
    , sick : Bool
    , stunned : Bool
    , confused : Bool
    , scarred : Bool
    }


initialModel : Model
initialModel =
    { strength = 10
    , dexterity = 10
    , constitution = 10
    , intelligence = 10
    , wisdom = 10
    , charisma = 10
    , damage = ""
    , damageError = False
    , armor = 0
    , currentHP = 0
    , maxHP = 0
    , weak = False
    , shaky = False
    , sick = False
    , stunned = False
    , confused = False
    , scarred = False
    }


type Msg
    = Strength Int
    | Dexterity Int
    | Constitution Int
    | Intelligence Int
    | Wisdom Int
    | Charisma Int
    | Damage String
    | Armor Int
    | CurrentHP Int
    | MaxHP Int
    | Weak
    | Shaky
    | Sick
    | Stunned
    | Confused
    | Scarred
    | RollDamage
    | RollStrength
    | RollDexterity
    | RollConstitution
    | RollIntelligence
    | RollWisdom
    | RollCharisma


type UpstreamMsg
    = RollDamageUp String
    | RollStrengthUp Int
    | RollDexterityUp Int
    | RollConstitutionUp Int
    | RollIntelligenceUp Int
    | RollWisdomUp Int
    | RollCharismaUp Int
    | NoneUp


update : Msg -> Model -> ( Model, Cmd Msg, UpstreamMsg )
update msg model =
    case msg of
        Strength value ->
            ( { model | strength = value }, Cmd.none, NoneUp )

        Dexterity value ->
            ( { model | dexterity = value }, Cmd.none, NoneUp )

        Constitution value ->
            ( { model | constitution = value }, Cmd.none, NoneUp )

        Intelligence value ->
            ( { model | intelligence = value }, Cmd.none, NoneUp )

        Wisdom value ->
            ( { model | wisdom = value }, Cmd.none, NoneUp )

        Charisma value ->
            ( { model | charisma = value }, Cmd.none, NoneUp )

        Damage value ->
            let
                error =
                    if String.length value > 0 then
                        case check value of
                            Ok _ ->
                                False

                            Err _ ->
                                True
                    else
                        False
            in
                ( { model | damage = value, damageError = error }
                , Cmd.none
                , NoneUp
                )

        Armor value ->
            ( { model | armor = value }, Cmd.none, NoneUp )

        CurrentHP value ->
            ( { model | currentHP = value }, Cmd.none, NoneUp )

        MaxHP value ->
            ( { model | maxHP = value }, Cmd.none, NoneUp )

        Weak ->
            ( { model | weak = not model.weak }, Cmd.none, NoneUp )

        Shaky ->
            ( { model | shaky = not model.shaky }, Cmd.none, NoneUp )

        Sick ->
            ( { model | sick = not model.sick }, Cmd.none, NoneUp )

        Stunned ->
            ( { model | stunned = not model.stunned }, Cmd.none, NoneUp )

        Confused ->
            ( { model | confused = not model.confused }, Cmd.none, NoneUp )

        Scarred ->
            ( { model | scarred = not model.scarred }, Cmd.none, NoneUp )

        RollDamage ->
            ( model, Cmd.none, RollDamageUp model.damage )

        RollStrength ->
            ( model
            , Cmd.none
            , adjustForDebility model.strength model.weak |> RollStrengthUp
            )

        RollDexterity ->
            ( model
            , Cmd.none
            , adjustForDebility model.dexterity model.shaky |> RollDexterityUp
            )

        RollConstitution ->
            ( model
            , Cmd.none
            , adjustForDebility model.constitution model.sick
                |> RollConstitutionUp
            )

        RollIntelligence ->
            ( model
            , Cmd.none
            , adjustForDebility model.intelligence model.stunned
                |> RollIntelligenceUp
            )

        RollWisdom ->
            ( model
            , Cmd.none
            , adjustForDebility model.wisdom model.confused |> RollWisdomUp
            )

        RollCharisma ->
            ( model
            , Cmd.none
            , adjustForDebility model.charisma model.scarred |> RollCharismaUp
            )


view : Model -> Html Msg
view model =
    div []
        [ abilities model ]


abilities : Model -> Html Msg
abilities model =
    div
        [ centerFlexRow
            ++ [ flexWrap Css.wrap
               , justifyContent spaceBetween
               ]
            |> styles
        ]
        [ div [ flex (int 1) :: centerFlexRowAround |> styles ]
            [ statBox Strength
                RollStrength
                Weak
                model.strength
                model.weak
                "Strength"
                "STR"
                "Weak"
            , statBox Dexterity
                RollDexterity
                Shaky
                model.dexterity
                model.shaky
                "Dexterity"
                "DEX"
                "Shaky"
            , statBox Constitution
                RollConstitution
                Sick
                model.constitution
                model.sick
                "Constitution"
                "CON"
                "Sick"
            ]
        , div [ flex (int 1) :: centerFlexRowAround |> styles ]
            [ statBox Intelligence
                RollIntelligence
                Stunned
                model.intelligence
                model.stunned
                "Intelligence"
                "INT"
                "Stunned"
            , statBox Wisdom
                RollWisdom
                Confused
                model.wisdom
                model.confused
                "Wisdom"
                "WIS"
                "Confused"
            , statBox Charisma
                RollCharisma
                Scarred
                model.charisma
                model.scarred
                "Charisma"
                "CHA"
                "Scarred"
            ]
        , div [ flex (int 1) :: centerFlexRowAround |> styles ]
            [ damage model.damage model.damageError
            , armor model.armor
            , hitPoints model
            ]
        ]


statBox :
    (Int -> Msg)
    -> Msg
    -> Msg
    -> Int
    -> Bool
    -> String
    -> String
    -> String
    -> Html Msg
statBox statMsg rollMsg debilityMsg statModel debilityModel name modifier debility =
    div
        [ stretchFlexColumn ++ [ marginTop (px 4) ] |> styles ]
        [ header
            [ class "w3-container w3-theme", styles [ textAlign center ] ]
            [ text name ]
        , div
            [ class "w3-border"
            , centerFlexRow ++ [ justifyContent spaceBetween ] |> styles
            ]
            [ grayInputNumber statMsg
                (Just 18)
                statModel
                (inputStyles 4
                    |> styles
                    |> Just
                )
            , div
                [ centerFlexColumn
                    ++ [ border3 (px 2) solid (rgb 6 6 6)
                       , borderRadius (pct 50)
                       , justifyContent center
                       , margin (px 8)
                       , Css.height (px 50)
                       , Css.width (px 50)
                       ]
                    |> styles
                ]
                [ span [] [ statModifier statModel |> toString |> text ]
                , span
                    [ styles [ fontSize (pct 70) ] ]
                    [ text modifier ]
                ]
            , random rollMsg
            ]
        , div
            [ class "w3-bar w3-theme" ]
            [ input
                [ checked debilityModel
                , class "w3-check"
                , checkboxStyles ++ [ marginLeft (px 4) ] |> styles
                , onClick debilityMsg
                , type_ "checkbox"
                ]
                []
            , label [ class "w3-small", styles [ marginLeft (px 8) ] ]
                [ text debility ]
            ]
        ]


armor : Int -> Html Msg
armor model =
    div
        abilityAttributes
        [ span [] [ text "Armor" ]
        , grayInputNumber Armor Nothing model (inputStyles 8 |> styles |> Just)
        ]


damage : String -> Bool -> Html Msg
damage model error =
    let
        styles_ =
            if error then
                (backgroundColor (rgb 255 192 203)) :: inputStyles 8
            else
                inputStyles 8
    in
        div
            abilityAttributes
            [ span [] [ text "Damage" ]
            , grayInputText Damage model (styles styles_ |> Just)
            , random RollDamage
            ]


hitPoints : Model -> Html Msg
hitPoints model =
    div
        abilityAttributes
        [ span [ styles [ marginRight (px 8) ] ] [ text "HP" ]
        , grayInputNumber CurrentHP
            Nothing
            model.currentHP
            (inputStyles 0
                |> styles
                |> Just
            )
        , span [ styles [ margin2 (px 0) (px 4) ] ] [ text "/" ]
        , grayInputNumber MaxHP
            Nothing
            model.maxHP
            (inputStyles 0
                |> styles
                |> Just
            )
        ]


abilityAttributes : List (Attribute Msg)
abilityAttributes =
    [ class "w3-margin-top w3-padding-small w3-theme"
    , styles
        [ alignItems center
        , displayFlex
        , flexDirection row
        , justifyContent spaceBetween
        , margin (px 4)
        ]
    ]


inputStyles : Float -> List Css.Style
inputStyles margin_ =
    [ maxHeight (ex 4), maxWidth (Css.em 4), margin2 (px 0) (px margin_) ]


adjustForDebility : Int -> Bool -> Int
adjustForDebility stat debility =
    (statModifier stat)
        - (if debility then
            1
           else
            0
          )


statModifier : Int -> Int
statModifier stat =
    if stat <= 3 then
        -3
    else if stat <= 5 then
        -2
    else if stat <= 8 then
        -1
    else if stat <= 12 then
        0
    else if stat <= 15 then
        1
    else if stat <= 17 then
        2
    else
        3


random : Msg -> Html Msg
random msg =
    a
        [ class "w3-button w3-tiny", onClick msg, styles [ padding (px 4) ] ]
        [ icon "random" ]
