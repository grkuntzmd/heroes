module DraggableList exposing (Model, Msg(..), initialModel, update, view)

import Css exposing (flexEnd, justifyContent)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Item
import Random.Pcg as Random
import Styles exposing (centerFlexRow, icon, stretchFlexColumn, styles)
import Uuid


type alias Model =
    { items : List Item.Model
    , seed : Random.Seed
    , drag : Maybe Drag
    }


initialModel : Model
initialModel =
    { items = []
    , seed = Random.initialSeed 0
    , drag = Nothing
    }


type alias Drag =
    { itemIndex : Int
    , startY : Int
    , currentY : Int
    }


type Msg
    = AddItem
    | ItemMsg String Item.Msg
    | InitializeSeed Random.Seed


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AddItem ->
            let
                ( uuid, seed ) =
                    Random.step Uuid.uuidGenerator model.seed

                item =
                    uuid |> Uuid.toString |> Item.initialModel
            in
                ( { model
                    | items = model.items ++ List.singleton item
                    , seed = seed
                  }
                , Cmd.none
                )

        ItemMsg uuid msg_ ->
            case Item.find uuid model.items of
                Just value ->
                    let
                        index =
                            Tuple.first value

                        item =
                            Tuple.second value

                        lastIndex =
                            List.length model.items - 1

                        ( model_, cmd, upstreamMsg ) =
                            Item.update msg_ item
                    in
                        case upstreamMsg of
                            Item.ReplaceItemUp ->
                                ( { model
                                    | items =
                                        Item.replace model_
                                            model.items
                                  }
                                , Cmd.map (ItemMsg uuid) cmd
                                )

                            Item.DeleteItemUp ->
                                ( { model
                                    | items =
                                        Item.remove model_
                                            model.items
                                  }
                                , Cmd.map (ItemMsg uuid) cmd
                                )

                            Item.RaiseUp ->
                                if index == 0 then
                                    ( model
                                    , Cmd.map (ItemMsg uuid) cmd
                                    )
                                else
                                    let
                                        items =
                                            (List.take (index - 1)
                                                model.items
                                            )
                                                ++ (List.drop (index - 1)
                                                        model.items
                                                        |> List.take 2
                                                        |> List.reverse
                                                   )
                                                ++ (List.drop
                                                        (index + 1)
                                                        model.items
                                                   )
                                    in
                                        ( { model | items = items }
                                        , Cmd.map (ItemMsg uuid) cmd
                                        )

                            Item.LowerUp ->
                                if index == lastIndex then
                                    ( model
                                    , Cmd.map (ItemMsg uuid) cmd
                                    )
                                else
                                    let
                                        items =
                                            (List.take index
                                                model.items
                                            )
                                                ++ (List.drop index
                                                        model.items
                                                        |> List.take 2
                                                        |> List.reverse
                                                   )
                                                ++ (List.drop
                                                        (index + 2)
                                                        model.items
                                                   )
                                    in
                                        ( { model | items = items }
                                        , Cmd.map (ItemMsg uuid) cmd
                                        )

                Nothing ->
                    ( model, Cmd.none )

        InitializeSeed seed ->
            ( { model | seed = seed }, Cmd.none )


view : Model -> Html Msg
view model =
    let
        indexed =
            List.indexedMap (,) model.items

        lastIndex =
            List.length indexed - 1
    in
        div
            []
            [ ul
                [ class "w3-padding w3-ul", styles stretchFlexColumn ]
                (List.map
                    (\i ->
                        let
                            index =
                                Tuple.first i

                            item =
                                Tuple.second i

                            disabledUp =
                                index == 0

                            disabledDown =
                                index == lastIndex
                        in
                            Html.map (ItemMsg item.uuid)
                                (Item.view item disabledUp disabledDown)
                    )
                    indexed
                )
            , div
                [ centerFlexRow ++ [ justifyContent flexEnd ] |> styles ]
                [ a
                    [ class "w3-border w3-button w3-circle w3-theme-dark w3-margin w3-ripple"
                    , onClick AddItem
                    ]
                    [ icon "plus" ]
                ]
            ]
