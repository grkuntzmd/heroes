module Background exposing (Model, Msg(..), initialModel, update, view)

import Css
    exposing
        ( alignItems
        , block
        , border3
        , borderRadius
        , center
        , column
        , display
        , displayFlex
        , ex
        , flexDirection
        , flexEnd
        , flexWrap
        , fontSize
        , justifyContent
        , lineHeight
        , margin
        , margin2
        , marginLeft
        , marginRight
        , maxHeight
        , maxWidth
        , minWidth
        , none
        , pct
        , px
        , rgb
        , row
        , solid
        , spaceAround
        , spaceBetween
        , stretch
        , textAlign
        )
import DraggableList
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import InitializeSeeds exposing (initializeSeeds)
import Random.Pcg as Random
import Styles
    exposing
        ( centerFlexRow
        , centerFlexRowBetween
        , checkboxStyles
        , chevronDown
        , chevronUp
        , icon
        , stretchFlexColumn
        , stretchFlexRow
        , styles
        )


type alias Model =
    { backgrounds : DraggableList.Model
    , backgroundExpand : Bool
    , moves : DraggableList.Model
    , movesExpand : Bool
    , spells : DraggableList.Model
    , spellsExpand : Bool
    }


initialModel : Model
initialModel =
    { backgrounds = DraggableList.initialModel
    , backgroundExpand = False
    , moves = DraggableList.initialModel
    , movesExpand = False
    , spells = DraggableList.initialModel
    , spellsExpand = False
    }


type Msg
    = AddBackground
    | BackgroundExpand
    | BackgroundsMsg DraggableList.Msg
    | AddMove
    | MovesExpand
    | MovesMsg DraggableList.Msg
    | AddSpell
    | SpellsExpand
    | SpellsMsg DraggableList.Msg
    | InitializeSeeds Random.Seed


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AddBackground ->
            let
                ( model_, cmd ) =
                    DraggableList.update DraggableList.AddItem model.backgrounds
            in
                ( { model | backgrounds = model_ }
                , Cmd.map BackgroundsMsg cmd
                )

        BackgroundExpand ->
            ( { model | backgroundExpand = not model.backgroundExpand }
            , Cmd.none
            )

        BackgroundsMsg msg_ ->
            let
                ( model_, cmd ) =
                    DraggableList.update msg_ model.backgrounds
            in
                ( { model | backgrounds = model_ }
                , Cmd.map BackgroundsMsg cmd
                )

        AddMove ->
            let
                ( model_, cmd ) =
                    DraggableList.update DraggableList.AddItem model.moves
            in
                ( { model | moves = model_ }
                , Cmd.map MovesMsg cmd
                )

        MovesExpand ->
            ( { model | movesExpand = not model.movesExpand }
            , Cmd.none
            )

        MovesMsg msg_ ->
            let
                ( model_, cmd ) =
                    DraggableList.update msg_ model.moves
            in
                ( { model | moves = model_ }
                , Cmd.map MovesMsg cmd
                )

        AddSpell ->
            let
                ( model_, cmd ) =
                    DraggableList.update DraggableList.AddItem model.spells
            in
                ( { model | spells = model_ }
                , Cmd.map SpellsMsg cmd
                )

        SpellsExpand ->
            ( { model | spellsExpand = not model.spellsExpand }
            , Cmd.none
            )

        SpellsMsg msg_ ->
            let
                ( model_, cmd ) =
                    DraggableList.update msg_ model.spells
            in
                ( { model | spells = model_ }
                , Cmd.map SpellsMsg cmd
                )

        InitializeSeeds seed ->
            let
                msgNames =
                    [ BackgroundsMsg << DraggableList.InitializeSeed
                    , MovesMsg << DraggableList.InitializeSeed
                    , SpellsMsg << DraggableList.InitializeSeed
                    ]
            in
                ( initializeSeeds seed msgNames update model
                , Cmd.none
                )


view : Model -> Html Msg
view model =
    div
        [ styles stretchFlexColumn ]
        [ section
            "Background"
            model.backgrounds
            BackgroundsMsg
            AddBackground
            BackgroundExpand
            (Just "Name racial moves as \"Race Class\" (e.g. \"Halfling Thief\")")
            model.backgroundExpand
        , section
            "Moves"
            model.moves
            MovesMsg
            AddMove
            MovesExpand
            Nothing
            model.movesExpand
        , section
            "Spells"
            model.spells
            SpellsMsg
            AddSpell
            SpellsExpand
            Nothing
            model.spellsExpand
        ]


section :
    String
    -> DraggableList.Model
    -> (DraggableList.Msg -> Msg)
    -> Msg
    -> Msg
    -> Maybe String
    -> Bool
    -> Html Msg
section title listModel itemMsg addMsg expandMsg hint expand =
    let
        hint_ =
            case hint of
                Just hint_ ->
                    span [ styles [ fontSize (pct 80) ] ] [ text hint_ ]

                Nothing ->
                    span [] []

        ( class_, icon_ ) =
            if expand then
                ( "", chevronUp )
            else
                ( "w3-hide", chevronDown )
    in
        div
            [ class "w3-border w3-margin-top"
            , styles stretchFlexColumn
            ]
            [ header
                [ class "w3-padding-small w3-theme"
                , styles centerFlexRowBetween
                ]
                [ span [] [ text title ]
                , hint_
                , button
                    [ class "w3-button w3-circle w3-padding-small w3-ripple w3-tiny"
                    , onClick expandMsg
                    ]
                    [ icon_ ]
                ]
            , div
                [ class class_ ]
                [ Html.map itemMsg (DraggableList.view listModel) ]
            ]
