module Header exposing (view)

import Css
    exposing
        ( alignItems
        , auto
        , center
        , column
        , display
        , displayFlex
        , em
        , ex
        , fixed
        , flex
        , flexDirection
        , hidden
        , inlineBlock
        , int
        , justifyContent
        , lineHeight
        , margin
        , marginTop
        , overflow
        , paddingRight
        , pct
        , position
        , px
        , row
        , spaceBetween
        , stretch
        , top
        , width
        )
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Model exposing (Model)
import Msg exposing (Msg(..))
import Styles
    exposing
        ( centerFlexRow
        , icon
        , marginLeftSmall
        , startFlexColumn
        , stretchFlexColumn
        , styles
        )


view : Model -> Html Msg
view model =
    nav
        [ class "w3-theme"
        , centerFlexRow
            ++ [ flex (int 0)
               , justifyContent spaceBetween
               , overflow Css.hidden
               , position fixed
               , top (px 0)
               , Css.width (pct 100)
               ]
            |> styles
        ]
        [ span
            [ styles centerFlexRow ]
            [ h3 [ class "w3-margin" ] [ icon "shield" ]
            , div
                [ styles startFlexColumn ]
                [ h3
                    [ styles
                        [ display inlineBlock
                        , lineHeight (ex 1)
                        ]
                    ]
                    [ text "Heroes" ]
                , p
                    [ styles
                        [ display inlineBlock
                        , lineHeight (ex 2)
                        , marginTop (Css.em 0.2)
                        ]
                    ]
                    [ text "Dungeon World Characters" ]
                ]
            ]
        , buttons model
        , characterSelector model
        , a
            [ class "w3-button w3-large w3-margin-right w3-padding-small" ]
            [ icon "bars" ]
        ]


buttons : Model -> Html Msg
buttons _ =
    topBarButton EditGlobalItems "Edit All Moves / Spells"


characterSelector : Model -> Html Msg
characterSelector _ =
    span
        [ centerFlexRow ++ [ paddingRight (px 10) ] |> styles ]
        [ h5
            [ class "w3-margin-right", styles [ display inlineBlock ] ]
            [ text "Character" ]
        , select
            [ class "w3-border w3-select", styles [ Css.width auto ] ]
            [ option
                [ value "1" ]
                [ text "Alton Braun (Halfling Thief)" ]
            , option
                [ value "2" ]
                [ text "Grulmack Rocknoggin (Dwarf Cleric)" ]
            ]
        , a
            [ class "w3-border w3-button w3-padding-small w3-ripple w3-round w3-theme-dark"
            , styles marginLeftSmall
            ]
            [ icon "plus" ]
        , a
            [ class "w3-border w3-button w3-padding-small w3-ripple w3-round w3-theme-dark"
            , styles marginLeftSmall
            ]
            [ icon "trash" ]
        ]


topBarButton : Msg -> String -> Html Msg
topBarButton msg label =
    a
        [ class "w3-border w3-button w3-padding-small w3-ripple w3-round w3-small w3-theme-dark"
        , onClick msg
        ]
        [ text label ]
