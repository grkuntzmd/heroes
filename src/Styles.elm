module Styles
    exposing
        ( centerFlexColumn
        , centerFlexRow
        , centerFlexRowAround
        , centerFlexRowBetween
        , checkboxStyles
        , chevronDown
        , chevronUp
        , icon
        , marginLeftSmall
        , marginTopSmall
        , startFlexColumn
        , startFlexRow
        , stretchFlexColumn
        , stretchFlexRow
        , styles
        )

import Css
    exposing
        ( Px
        , Style
        , alignItems
        , asPairs
        , center
        , column
        , displayFlex
        , flexDirection
        , flexStart
        , justifyContent
        , marginLeft
        , marginTop
        , px
        , row
        , spaceAround
        , spaceBetween
        , stretch
        , top
        )
import Html exposing (Attribute, Html)
import Html.Attributes exposing (class, style)


small : Px
small =
    (px 6)


icon : String -> Html msg
icon name =
    Html.i [ "fa fa-" ++ name |> class ] []


chevronDown : Html msg
chevronDown =
    icon "chevron-down"


chevronUp : Html msg
chevronUp =
    icon "chevron-up"


styles : List Style -> Attribute msg
styles =
    asPairs >> style


checkboxStyles : List Style
checkboxStyles =
    [ Css.height (px 16), Css.width (px 16), top (px 4) ]


centerFlex : List Style
centerFlex =
    [ alignItems center, displayFlex ]


centerFlexColumn : List Style
centerFlexColumn =
    flexDirection column :: justifyContent spaceBetween :: centerFlex


centerFlexRow : List Style
centerFlexRow =
    flexDirection row :: centerFlex


centerFlexRowAround : List Style
centerFlexRowAround =
    flexDirection row :: justifyContent spaceAround :: centerFlex


centerFlexRowBetween : List Style
centerFlexRowBetween =
    justifyContent spaceBetween :: centerFlexRow


startFlex : List Style
startFlex =
    [ alignItems flexStart, displayFlex ]


startFlexColumn : List Style
startFlexColumn =
    flexDirection column :: startFlex


startFlexRow : List Style
startFlexRow =
    flexDirection row :: startFlex


stretchFlex : List Style
stretchFlex =
    [ alignItems stretch, displayFlex ]


stretchFlexColumn : List Style
stretchFlexColumn =
    flexDirection column :: stretchFlex


stretchFlexRow : List Style
stretchFlexRow =
    flexDirection row :: stretchFlex


marginLeftSmall : List Style
marginLeftSmall =
    [ marginLeft small ]

marginTopSmall : List Style
marginTopSmall =
    [ marginTop small ]
