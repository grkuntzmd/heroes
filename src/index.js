"use strict";

require("font-awesome/css/font-awesome.css");
require("w3-css/w3.css");
require("./w3-theme-red.css");
require("./index.html");

const Elm = require("./Main.elm");
Elm.Main.fullscreen();